package datalake.lambda;

import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.events.KinesisAnalyticsInputPreprocessingResponse;
import com.amazonaws.services.lambda.runtime.events.KinesisAnalyticsInputPreprocessingResponse.Record;
import com.amazonaws.services.lambda.runtime.events.KinesisAnalyticsInputPreprocessingResponse.Result;
import com.amazonaws.services.lambda.runtime.events.KinesisFirehoseEvent;
import com.google.protobuf.util.JsonFormat;
import datalake.schema.ActivityProtos.*;

public class LambdaFirehose {
    public KinesisAnalyticsInputPreprocessingResponse handler(final KinesisFirehoseEvent event, final Context context) {

        LambdaLogger logger = context.getLogger();
        logger.log(context.getAwsRequestId() + " handler\n");

        List<Record> records = event.getRecords().stream().map(record -> processActivityRecord(record, logger))
                .collect(Collectors.toList());
        KinesisAnalyticsInputPreprocessingResponse response = new KinesisAnalyticsInputPreprocessingResponse(records);
        return response;
    }

    private Record processActivityRecord(KinesisFirehoseEvent.Record record, LambdaLogger logger) {
        Record firehoseRecord = new KinesisAnalyticsInputPreprocessingResponse.Record();
        try {
            firehoseRecord.setRecordId(record.getRecordId());
            firehoseRecord.setResult(Result.Ok);
            Activity activity = Activity.parseFrom(record.getData());
            String str = JsonFormat.printer().print(activity);
            logger.log(str);
            byte[] data = Base64.getEncoder().encode(str.getBytes());
            firehoseRecord.setData(ByteBuffer.wrap(data));
        } catch (Exception e) {
            logger.log(record.getRecordId() + " " + e.getMessage());
            firehoseRecord.setResult(Result.Dropped);
        }
        return firehoseRecord;
    }
}