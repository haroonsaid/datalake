import { Buffer } from "buffer";
import { Context, FirehoseTransformationResultRecord, FirehoseTransformationEventRecord, FirehoseTransformationEvent, FirehoseTransformationResult } from "aws-lambda";
import { Activity } from "./activity_pb";

export class LambdaFirehose {
  public handler(event: FirehoseTransformationEvent, context: Context): FirehoseTransformationResult {
    console.info(`${context.awsRequestId} message recieved`);
    const records = event.records.map(record => this.processRecord(record));
    return { records: records };
  }
  private processRecord(record: FirehoseTransformationEventRecord): FirehoseTransformationResultRecord {
    const payload = new Buffer(record.data, "base64");
    const message = Activity.deserializeBinary(payload);
    const str = JSON.stringify(message.toObject);
    console.info(`message:${str}`);
    return { recordId: record.recordId, result: "Ok", data: str };
  }
}
