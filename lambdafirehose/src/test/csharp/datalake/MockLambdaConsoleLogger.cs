using System;
using Amazon.Lambda.Core;

namespace Lambda
{
    public class MockLambdaConsoleLogger : ILambdaLogger
    {

        //-------------------------------------------------------------
        // Implementation - LambdaLogger
        //-------------------------------------------------------------

        public void Log(string s)
        {
            Console.Write(s);
        }

        public void LogLine(string message)
        {
            Console.WriteLine(message);
        }
    }
}