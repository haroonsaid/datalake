using System;
using Xunit;
using Moq;
using Amazon.Lambda.Core;
using Newtonsoft.Json;
using Datalake.Schema;
using System.Collections.Generic;
using Datalake.Lambda;
using Amazon.Lambda.KinesisFirehoseEvents;
using System.Linq;
using System.IO;
using Google.Protobuf;
using System.Text;

namespace Lambda
{
    public class LambdaFirehoseTests
    {
        [Fact]
        public void SuccessfulResponse()
        {
            var lambda = new LambdaFirehose();
            var kinesisFirehoseEvent = new KinesisFirehoseEvent();
            kinesisFirehoseEvent.DeliveryStreamArn = "arn::junk";
            kinesisFirehoseEvent.Region = "US-EAST-2";
            var record = createFakeRecord();
            kinesisFirehoseEvent.Records = new List<KinesisFirehoseEvent.FirehoseRecord>() { { record } };
            var mockContext = new Mock<ILambdaContext>();
            mockContext.Setup(p => p.Logger).Returns(new MockLambdaConsoleLogger());
            var actual = lambda.Handler(kinesisFirehoseEvent, mockContext.Object);
            Assert.NotEmpty(actual.Records);
            var acutalRecord = actual.Records.FirstOrDefault();
            Assert.Equal(KinesisFirehoseResponse.TRANSFORMED_STATE_OK, acutalRecord.Result);
        }

        [Fact]
        public void DroppedResponse()
        {
            LambdaFirehose lambda = new LambdaFirehose();
            var kinesisFirehoseEvent = new KinesisFirehoseEvent();
            kinesisFirehoseEvent.DeliveryStreamArn = "arn::junk";
            kinesisFirehoseEvent.Region = "US-EAST-2";
            var record = createInvalidRecord();
            kinesisFirehoseEvent.Records = new List<KinesisFirehoseEvent.FirehoseRecord>() { { record } };
            var mockContext = new Mock<ILambdaContext>();
            mockContext.Setup(p => p.Logger).Returns(new MockLambdaConsoleLogger());
            var actual = lambda.Handler(kinesisFirehoseEvent, mockContext.Object);
            Assert.NotEmpty(actual.Records);
            var acutalRecord = actual.Records.FirstOrDefault();
            Assert.Equal(KinesisFirehoseResponse.TRANSFORMED_STATE_DROPPED, acutalRecord.Result);
        }

        private KinesisFirehoseEvent.FirehoseRecord createFakeRecord()
        {
            Activity activity = createFakeActivity();
            var bytes = Serialize(activity);
            var record = new KinesisFirehoseEvent.FirehoseRecord()
            {
                RecordId = "Rec1",
                ApproximateArrivalEpoch = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                Base64EncodedData = Convert.ToBase64String(bytes),
            };
            return record;
        }
        internal Byte[] Serialize(Activity record)
        {
            using (var ms = new MemoryStream())
            {
                record.WriteTo(ms);
                return ms.ToArray();
            }
        }
        private KinesisFirehoseEvent.FirehoseRecord createInvalidRecord()
        {
            var bytes = Encoding.Default.GetBytes("junk");
            var record = new KinesisFirehoseEvent.FirehoseRecord()
            {
                RecordId = "Rec1",
                ApproximateArrivalEpoch = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                Base64EncodedData = Convert.ToBase64String(bytes),
            };
            return record;
        }
        private Activity createFakeActivity()
        {
            var activity = new Activity
            {
                UserId = Guid.NewGuid().ToString(),
                Timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(),
                ActivityType = Activity.Types.ActivityType.Account,
                AccountActivity = new AccountActivity
                {
                    AccountActivityType = AccountActivity.Types.AccountActivityType.Created,
                    AccountId = Guid.NewGuid().ToString(),
                    AccountType = AccountActivity.Types.AccountType.Retirement
                }

            };
            return activity;
        }
    }
}
