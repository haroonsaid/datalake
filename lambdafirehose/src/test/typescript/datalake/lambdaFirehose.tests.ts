import { LambdaFirehose } from "../../../main/typescript/datalake/lambda/LambdaFirehose";
import {
  FirehoseTransformationEvent,
  FirehoseTransformationEventRecord,
  Context
} from "aws-lambda";

const createFakeRecord = (): FirehoseTransformationEventRecord => {
  const rec = {
    recordId: "record-1",
    approximateArrivalTimestamp: 0,
    /** Base64 encoded */
    data: "theprotobuf"
  };
  return rec;
};

describe("Datalake Tests", () => {
  describe("Firehouse Tests", () => {
    test("should succeed", done => {
      const lambda = new LambdaFirehose();
      const fakeRecords = [createFakeRecord()];
      const event: FirehoseTransformationEvent = {
        invocationId: "recid",
        deliveryStreamArn: "string",
        region: "string",
        records: fakeRecords
      };
      const fakeContext = jest.fn<Context>(() => {});
      const context = new fakeContext();
      const result = lambda.handler(event, context);
      expect(result).toBe(true);
      done();
    });
  });
});
