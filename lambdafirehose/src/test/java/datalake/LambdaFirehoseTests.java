package datalake;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.amazonaws.services.lambda.runtime.events.KinesisFirehoseEvent;
import com.amazonaws.services.lambda.runtime.Context;
import static org.mockito.Mockito.*;

import java.nio.ByteBuffer;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.UUID;

import org.junit.Test;

import datalake.lambda.LambdaFirehose;
import datalake.schema.ActivityProtos.AccountActivity;
import datalake.schema.ActivityProtos.Activity;
import datalake.schema.ActivityProtos.Activity.ActivityType;
import com.amazonaws.services.lambda.runtime.events.KinesisAnalyticsInputPreprocessingResponse;

public class LambdaFirehoseTests {
    @Test
    public void successfulResponse() {
        LambdaFirehose lambda = new LambdaFirehose();
        KinesisFirehoseEvent event = new KinesisFirehoseEvent();
        event.setDeliveryStreamArn("arn::junk");
        event.setRegion("US-EAST-2");
        KinesisFirehoseEvent.Record record = createFakeRecord();
        event.setRecords(new ArrayList<>(Arrays.asList(record)));
        MockLambdaConsoleLogger logger = new MockLambdaConsoleLogger();
        Context mockContext = mock(Context.class);
        when(mockContext.getLogger()).thenReturn(logger);
        KinesisAnalyticsInputPreprocessingResponse actual = lambda.handler(event, mockContext);
        assertNotNull(actual);
        assertEquals(1, actual.records.size());
        KinesisAnalyticsInputPreprocessingResponse.Record acutalRecord = actual.records.get(0);
        assertEquals(KinesisAnalyticsInputPreprocessingResponse.Result.Ok, acutalRecord.result);
    }

    @Test
    public void droppedResponse() {
        LambdaFirehose lambda = new LambdaFirehose();
        KinesisFirehoseEvent event = new KinesisFirehoseEvent();
        event.setDeliveryStreamArn("arn::junk");
        event.setRegion("US-EAST-2");
        KinesisFirehoseEvent.Record record = createInvalidRecord();
        record.setRecordId("recordId");
        event.setRecords(new ArrayList<>(Arrays.asList(record)));
        MockLambdaConsoleLogger logger = new MockLambdaConsoleLogger();
        Context mockContext = mock(Context.class);
        when(mockContext.getLogger()).thenReturn(logger);
        KinesisAnalyticsInputPreprocessingResponse actual = lambda.handler(event, mockContext);
        assertNotNull(actual);
        assertEquals(1, actual.records.size());
        KinesisAnalyticsInputPreprocessingResponse.Record acutalRecord = actual.records.get(0);
        assertEquals(KinesisAnalyticsInputPreprocessingResponse.Result.Dropped, acutalRecord.result);
    }

    private KinesisFirehoseEvent.Record createFakeRecord() {
        Activity activity = createFakeActivity();
        KinesisFirehoseEvent.Record record = new KinesisFirehoseEvent.Record();
        String recordId = "rec1";
        record.setRecordId(recordId);
        record.setData(ByteBuffer.wrap(activity.toByteArray()));
        long timestamp = toEpochMilli(LocalDateTime.now());
        record.setApproximateArrivalTimestamp(timestamp);
        record.setApproximateArrivalTimestamp(timestamp);
        return record;
    }

    private KinesisFirehoseEvent.Record createInvalidRecord() {
        KinesisFirehoseEvent.Record record = new KinesisFirehoseEvent.Record();
        String recordId = "rec1";
        record.setRecordId(recordId);
        String str = "Some data ";
        byte[] bytes = Base64.getEncoder().encode(str.getBytes());
        record.setData(ByteBuffer.wrap(bytes));
        long timestamp = toEpochMilli(LocalDateTime.now());
        record.setApproximateArrivalTimestamp(timestamp);
        record.setApproximateArrivalTimestamp(timestamp);
        return record;
    }

    private long toEpochMilli(LocalDateTime localDateTime) {
        return localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    private Activity createFakeActivity() {
        String guid = UUID.randomUUID().toString();
        AccountActivity accountActivity = AccountActivity.newBuilder()
                .setAccountActivityTypeValue(1000)
                .setAccountId("123456789")
                .setAccountType(AccountActivity.AccountType.MORTAGE)
                .build();
        Activity activity = Activity.newBuilder()
                .setUserId(guid)
                .setTimestamp(toEpochMilli(LocalDateTime.now()))
                .setActivityType(ActivityType.Account)
                .setAccountActivity(accountActivity)
                .build();
        return activity;
    }
}