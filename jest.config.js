module.exports = {
  roots: ["./lambdafirehose/src/test"],
  transform: {
    "^.+\\.ts$": "ts-jest"
  },
  testRegex: ".*tests\\.ts$",
  moduleFileExtensions: ["ts", "js", "node"]
};
