package datalake.kinesisactivityclient;
import datalake.schema.ActivityProtos;

public interface Activity {
    void recordAsync(ActivityProtos.Activity record, String partitionKey);
}